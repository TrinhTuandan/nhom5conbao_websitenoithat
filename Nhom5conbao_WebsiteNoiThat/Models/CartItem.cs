﻿namespace Nhom5conbao_WebsiteNoiThat.Models
{
    [Serializable]
    public class CartItem
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
