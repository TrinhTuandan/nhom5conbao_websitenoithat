﻿using Nhom5conbao_WebsiteNoiThat.Models;
using Nhom5conbao_WebsiteNoiThat.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Nhom5conbao_WebsiteNoiThat.Models;

namespace Nhom5conbao_WebsiteNoiThat.Controllers
{
    public class PostsController : Controller
    {
        private readonly WebNoiThatContext _context;
        public PostsController(WebNoiThatContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            var menus = await _context.Menus.Where(m => m.Hide == 0).OrderBy(m => m.Order).ToListAsync();
            var blogs = await _context.Blogs.Where(m => m.Hide == 0).OrderBy(m => m.Order).ToListAsync();
            var viewModel = new PostsViewModel
            {
                Menus = menus,
                Blogs = blogs,
            };
            return View(viewModel);
        }
        public async Task<IActionResult> _MenuPartial()
        {
            return PartialView();
        }
        public async Task<IActionResult> _BlogPartial()
         {
             return PartialView();
         }
        // Thêm mới ArticleDetails để ArticleDetails.cshtml sử dụng để xem Chi Tiết Bài Viết chi-tiet-bai-viet
        //------------------------------
        public async Task<IActionResult> ArticleDetails(string slug, long id)
        {
            var menus = await _context.Menus.Where(m => m.Hide == 0).OrderBy(m => m.Order).ToListAsync();
            var blog = await _context.Blogs.FirstOrDefaultAsync(m => m.Link == slug && m.IdBlog == id);

            if (blog == null)
            {
                var errorViewModel = new ErrorViewModel
                {
                    RequestId = "Product Error",
                };
                return View("Error", errorViewModel);
            }

            var viewModel = new PostsViewModel
            {
                Menus = menus,
                Blogs = new List<Blog> { blog },
            };
            return View(viewModel);
        }
    }
}