﻿using Nhom5conbao_WebsiteNoiThat.Models;

namespace Nhom5conbao_WebsiteNoiThat.ViewModels
{
    public class ProductViewModel
    {
        public List<Menu> Menus { get; set; }
        public List<Blog> Blogs { get; set; }
        public List<Product> Prods { get; set; }
        public String cateName { get; set; }
    }
}
