﻿using Nhom5conbao_WebsiteNoiThat.Models;

namespace Nhom5conbao_WebsiteNoiThat.ViewModels
{
    public class CartViewModel
    {
        public List<Menu> Menus { get; set; }
        public List<Blog> Blogs { get; set; }
        public List<CartItem> CartItems { get; set; }

        public int PageNumber { get; set; }
        public int TotalPages { get; set; }
    }
}
