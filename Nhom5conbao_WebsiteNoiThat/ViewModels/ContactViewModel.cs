﻿using Nhom5conbao_WebsiteNoiThat.Models;

namespace Nhom5conbao_WebsiteNoiThat.ViewModels
{
    public class ContactViewModel
    {
        public List<Menu> Menus { get; set; }
        public List<Blog> Blogs { get; set; }

    }
}
