﻿using Nhom5conbao_WebsiteNoiThat.Models;

namespace Nhom5conbao_WebsiteNoiThat.ViewModels
{
    public class HomeViewModel
    {
        public List<Menu> Menus { get; set; }
        public List<Blog> Blogs { get; set; }
        public List<Slider> Sliders { get; set; }
        public List<Product> CatProds { get; set; }
        public List<Product> DogProds { get; set; }
        public Catology SanPhamGheNgoi { get; set; }
        public Catology SanphamBanLamViec { get; set; }
    }
}
